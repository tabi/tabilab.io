## Quick Start

Home of the Tabi Project, an open source project for data collection through mobile phones.

Read about the [project and its mission](page/about), or head to [gitlab.com/tabi](https://gitlab.com/tabi) to see the code. It contains the [app itself](https://gitlab.com/tabi/tabi-app), the [backend](https://gitlab.com/tabi-backend) to collect measured data and a [documentation](https://gitlab.com/tabi-documentation) folder. [This website](https://gitlab.com/tabi.gitlab.io) can be found there as well, for the sake of transparency.
