---
title: Tabi Goes Open Source
subtitle: Hello World!
date: 2017-11-21
tags: ["hello world", "development update"]
---

The first step of a journey. As of today, the source code for the Tabi App is open source under the [MIT license](https://tldrlegal.com/license/mit-license).
<!--more-->

Read about the [project and its mission](page/about), or head to [gitlab.com/tabi](https://gitlab.com/tabi) to see the code. It contains the [app itself](https://gitlab.com/tabi/tabi-app), the [backend](https://gitlab.com/tabi-backend) to collect measured data and a [documentation](https://gitlab.com/tabi-documentation) folder. [This website](https://gitlab.com/tabi.gitlab.io) can be found there as well, for the sake of transparency.

You can stay up to date by subscribing to the [RSS feed](/index.xml).
